package dev.kafka.microservice.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "wikimedia_recent_change")
@Data
public class Wikimedia {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    private String wikiEventData;
}
