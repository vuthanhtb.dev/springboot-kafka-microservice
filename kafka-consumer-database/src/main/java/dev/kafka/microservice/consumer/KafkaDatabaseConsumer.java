package dev.kafka.microservice.consumer;

import dev.kafka.microservice.entity.Wikimedia;
import dev.kafka.microservice.repository.WikimediaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaDatabaseConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaDatabaseConsumer.class);

    private final WikimediaRepository repository;

    public KafkaDatabaseConsumer(WikimediaRepository repository) {
        this.repository = repository;
    }

    @KafkaListener(
            topics = "${spring.kafka.topic.name}",
            groupId = "${spring.kafka.consumer.group-id}"
    )

    public void consume(String eventMessage){
        LOGGER.info(String.format("Event message received -> %s", eventMessage));
        Wikimedia wikimedia = new Wikimedia();
        wikimedia.setWikiEventData(eventMessage);
        this.repository.save(wikimedia);
    }
}
